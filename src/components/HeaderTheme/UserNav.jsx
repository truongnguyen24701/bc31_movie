import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { localStoregeServ } from '../../services/localStoregeService';
import { loginAction } from './../../redux/action/userAction';

export default function UserNav() {
    let dispatch = useDispatch()
    let userInfor = useSelector((state) => {
        return state.userReducer.userInfor
    })

    const renderContent = () => {
        if (userInfor) {
            return <div className='flex space-x-5 items-center'>
                <p>{userInfor.hoTen}</p>
                <button
                    onClick={handleLogout}
                    className='border-red-500 text-red-500 rounded px-5 py-3 border-2'>Đăng xuất</button>
            </div>
        } else {
            return <div div className='flex space-x-5 items-center'>
                <button
                    onClick={() => {
                        window.location.href = "/login"
                    }}
                    className='border-red-500 text-red-500 rounded px-5 py-3 border-2'>Đăng nhập</button>
                <button className='border-blue-500 text-blue-500 rounded px-5 py-3 border-2'>Đăng ký</button>
            </div>
        }
    }

    const handleLogout = () => {
        localStoregeServ.user.remove()
        dispatch(loginAction(null))
    }
    return (<div>{renderContent()}
    </div>)
}
