import React from 'react'
import { SyncLoader } from 'react-spinners'
import { useSelector } from 'react-redux';
import { spinnerReducer } from './../../redux/reducer/spinnerReducer';

export default function SpinnerComponent() {
    let { isLoading } = useSelector((state) => {
        return state.spinnerReducer
    })
    return isLoading ? (
        <div
            style={{ backgroundColor: "#5F6F94" }}
            className='h-screen w-screen fixed top-0 left-0 flex justify-center items-center z-50'>
            <SyncLoader color="#36d7b7" />
        </div>
    ) : (
        " "
    );
}
