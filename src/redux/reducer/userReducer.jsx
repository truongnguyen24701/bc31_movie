import { localStoregeServ } from "../../services/localStoregeService";

let initalState = {
    userInfor: localStoregeServ.user.get(),

}

export let userReducer = (state = initalState, action) => {
    switch (action.type) {
        case "LOGIN": {
            state.userInfor = action.payload;
            return { ...state }
        }
        default:
            return state
    }
}