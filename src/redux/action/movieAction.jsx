import { movieService } from '../../services/movie.service'
import { SET_MOVIE_LIST } from '../constants/movieConstants';

export const getMovieListActionServ = () => {
    return (dispatch) => {
        movieService
            .getMovieList()
            .then((res) => {
                console.log(res);
                dispatch({
                    type: SET_MOVIE_LIST,
                    payload: res.data.content,
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}