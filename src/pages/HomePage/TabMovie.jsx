import React from 'react';
import { Tabs } from 'antd';
import { useEffect, useState } from 'react';
import { movieService } from '../../services/movie.service';
import { data } from 'autoprefixer'
import ItemTabMovie from './ItemTabMovie';
import SpinnerComponent from '../../components/SpinnerComponent/SpinnerComponent';
const { TabPane } = Tabs;

export default function TabMovie() {
    const [dataMovie, setDataMovie] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    useEffect(() => {
        // setIsLoading(true)
        movieService
            .getMovieByTheater()
            .then((res) => {
                // setIsLoading(false)
                console.log(res);
                setDataMovie(res.data.content)
            })
            .catch((err) => {
                // setIsLoading(false)
                console.log(err);
            })
    }, [])
    const onChange = (key) => {
        console.log(key);
    };
    const renderContent = () => {
        return dataMovie.map((heThongRap, index) => {
            return (
                <TabPane tab={<img className='w-20' src={heThongRap.logo} />}
                    key={index}
                >
                    <Tabs style={{ height: 500 }} tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                        {/* React spinner */}
                        {heThongRap.lstCumRap.map((cumRap, index) => {
                            return (
                                <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                                    <div style={{ height: 500, overflow: "scroll" }} >
                                        {cumRap.danhSachPhim.map((phim) => {
                                            return <ItemTabMovie phim={phim} />
                                        })}
                                    </div>
                                </TabPane>
                            )
                        })}

                    </Tabs>
                </TabPane>
            )
        })
    }

    const renderTenCumRap = (cumRap) => {
        return <div className='text-left w-60 truncate'>
            <p className='text-green-700'>{cumRap.tenCumRap}</p>
            <p className='truncate'>{cumRap.diaChi}</p>
            <button className='text-red-600'>[ Xem chi tiết ]</button>
        </div>
    }

    return (
        <div className='container m-auto pb-96'>
            {/* {isLoading ? <SpinnerComponent /> : ""} */}
            <Tabs
                style={{ height: 500 }}
                tabPosition='left' defaultActiveKey="1" onChange={onChange}>
                {renderContent()}
            </Tabs>
        </div>
    )
}
