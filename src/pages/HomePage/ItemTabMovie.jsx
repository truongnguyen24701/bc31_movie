import React from 'react'
import moment from 'moment'

export default function ItemTabMovie({ phim }) {
    return (
        <div className='flex space-x-10 p-5'>
            <img className='w-24 h-48 object-cover my-3' src={phim.hinhAnh} alt="" />
            <div>
                <p className='font-medium text-2xl text-gray-800'>{phim.tenPhim}</p>
                <div className="grid grid-cols-4 gap-4">
                    {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu) => {
                        return <div className='bg-red-600 text-white p-2 rounded'>
                            Ngày: {moment(lichChieu.ngayChieuGioChieu).format("DD-MM-YYYY")}

                            <p className='text-yellow-400 font-medium'>
                                Giờ: {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                            </p>
                        </div>
                    })}</div>
            </div>
        </div>
    )
}
