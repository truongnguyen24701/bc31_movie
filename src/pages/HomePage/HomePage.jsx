import React, { Component } from 'react'
import ListMovie from './ListMovie'
import TabMovie from './TabMovie'

export default class HomePage extends Component {
  render() {
    return (
      <div className='space-y-20'>
        <ListMovie />
        <TabMovie />
      </div>
    )
  }
}
