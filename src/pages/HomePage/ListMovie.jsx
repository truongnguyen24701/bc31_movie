import React from 'react'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getMovieListActionServ } from '../../redux/action/movieAction';
import Meta from 'antd/lib/card/Meta';
import { Card } from 'antd';
import { NavLink } from 'react-router-dom';

export default function ListMovie() {
    let dispatch = useDispatch();

    let { movieList } = useSelector((state) => {
        return state.movieReducer;
    })
    console.log("movieList", movieList);
    useEffect(() => {
        // movieService
        //     .getMovieList()
        //     .then((res) => {
        //         console.log(res);
        //     })
        //     .catch((err) => {
        //         console.log(err);
        //     })

        dispatch(getMovieListActionServ());
    }, [])
    let renderMovieList = () => {
        return movieList
            // .filter((item) => {
            //     return item.hinhAnh !== null
            // })
            .map((item) => {
                console.log("item", item);
                return (
                    <Card
                        hoverable
                        style={{
                            width: "100%",
                        }}
                        className="shadow-xl hover:shadow-slate-800 hover:shadow-xl"
                        cover={
                            <img
                                style={{ width: "100%", height: "500px", borderRadius: "7px", objectFit: "cover" }}
                                alt=""
                                src={item.hinhAnh} />
                        }
                    >
                        <Meta title={<p className='text-blue-500'>{item.tenPhim}</p>} description="dasd" />
                        <NavLink to={`detail/${item.maPhim}`}>
                            <button className='w-full bg-red-600 text-white rounded text-xl font-medium py-3 my-3'>Mua vé</button>
                        </NavLink>
                    </Card>
                )
            })

            
    }
    return <div className='grid grid-cols-4 gap-10 mx-20 my-5'>{renderMovieList()}</div>

}
