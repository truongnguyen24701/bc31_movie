import React from 'react'
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { movieService } from './../../services/movie.service';
import { Progress } from 'antd'

export default function DetailPage() {
    let { id } = useParams()
    const [movie, setMovie] = useState({})
    useEffect(() => {
        movieService
            .getMovieDetail(id)
            .then((res) => {
                setMovie(res.data.content)
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            })
    }, []);
    return <div className='container mx-auto'>
        <div className="flex justify-center items-center py-10 space-x-10">
            <img src={movie.hinhAnh} className="w-60" alt="" />
            <p className='text-2xl font-medium text-red-500'>{movie.biDanh}</p>
            <Progress
                type="circle"
                percent={movie.danhGia * 10}
                format={(number) => {
                    return (
                        <span className='text-black font-medium'>{number / 10} Điểm</span>
                    )
                }} />
        </div>
    </div>
}
