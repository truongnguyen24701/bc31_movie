import './App.css';
import LoginPage from './pages/LoginPage/LoginPage';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import HomePage from './pages/HomePage/HomePage';
import Layout from './HOC/Layout';
import DetailPage from './pages/DetailPage/DetailPage';
import SpinnerComponent from './components/SpinnerComponent/SpinnerComponent';

function App() {
  return (
    <div>
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout Component={HomePage} />} />
          <Route path='/detail/:id' element={<Layout Component={DetailPage} />} />
          <Route path='/login' element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div >
  );
}

export default App;
