import React from 'react'
import HeaderTheme from '../components/HeaderTheme/HeaderTheme'

export default function Layout({ Component }) {
    return (
        <div>
            <HeaderTheme />
            <Component />
        </div>
    )
}
